<?php

/**
 * Theme callback for disabled plugins.
 * @todo should render the label
 * @todo provide classes that match normal links.
 */
function theme_ilink_disabled($variables) {
  return '<span>placeholder link</span>';
}
